// add keyup event listener to both first name and last name inputs
const firstNameInput = document.getElementById("txt-first-name");
const lastNameInput = document.getElementById("txt-last-name");
const fullNameSpan = document.getElementById("span-full-name");

// add keyup event listener to both first name and last name inputs
firstNameInput.addEventListener("keyup", updateFullName);
lastNameInput.addEventListener("keyup", updateFullName);

function updateFullName() {
  const firstName = firstNameInput.value.trim();
  const lastName = lastNameInput.value.trim();

  // set the text of the full name span to either the first name, last name, or both
  if (firstName && lastName) {
    fullNameSpan.textContent = `${firstName} ${lastName}`;
  } else if (firstName) {
    fullNameSpan.textContent = firstName;
  } else if (lastName) {
    fullNameSpan.textContent = lastName;
  } else {
    fullNameSpan.textContent = "";
  }
}
